
CCFLAGS := -Wall -Wextra -pedantic -std=c99 -g

all: program1 program2

program1: main.c MAGIC1/magic.c 
	gcc $^ -o $@ $(CCFLAGS) -DPROGRAM1

program2: main.c MAGIC2/magic.c
	gcc $^ -o $@ $(CCFLAGS) -DPROGRAM2

randomfile:
	dd if=/dev/urandom of=$@ count=100000

profile: performance.sh program1 program2 randomfile
	./performance.sh > $@ 2>&1

plots: plot.png plot2.png

plot.png: plot.gnuplot perf.dat
	./$<

plot2.png: plot2.gnuplot perf2.dat
	./$<

perf.dat perf2.dat: plot_perf.sh program1 program2 randomfile
	./$^

profile1: profile
	egrep "^1:" $^ | sed "s/1: //g" > $@

profile2: profile
	egrep "^2:" $^ | sed "s/2: //g"  > $@

.PHONY: unit_test valgrind_test check_test performance_test

unit_test: program1 program2
	./program1 -u
	./program2 -u

valgrind_test: program1 program2
	valgrind ./program1 -u
	valgrind ./program2 -u

check_test: program1 program2 randomfile
	-./program1 -a10 -m10 -c
	-./program2 -a10 -m10 -c
	-./program1 -a32 -m100000 -c
	-./program2 -a32 -m100000 -c

performance_test: program1 program2 randomfile
	./program1 -a4 -m1000 -b1000
	./program1 -a4 -m10000 -b1000
	./program1 -a4 -m100000 -b1000
	./program1 -a4 -m1000000 -b1000
	./program2 -a4 -m1000 -b1000
	./program2 -a4 -m10000 -b1000
	./program2 -a4 -m100000 -b1000
	./program2 -a4 -m1000000 -b1000

.PHONY: clean

clean:
	- rm -f program1 program2 randomfile profile profile1 profile2 perf.dat perf2.dat plot.png plot2.png
