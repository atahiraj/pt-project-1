#include "magic.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct node *link;

struct node {
    char c;
    int index;
    link low, eq, high;
};

struct trie {
    struct node *nodes;
    int nodes_index;
    int addr_index;
    int addr_size;
    int max_size;
};

/**
 * @brief alloc a node in the allocated block of the trie
 * 
 * @param t the trie with the memory block from which a node is allocated
 */
static inline struct node *alloc_node(struct trie *t)
{
    if (t->addr_index == t->max_size)
        return NULL;
    struct node *alloced = t->nodes + t->nodes_index;
    alloced->low = NULL;
    alloced->eq = NULL;
    alloced->high = NULL;
    t->nodes_index++;
    return alloced;
}

/**
 * @brief Initialize the magic
 * 
 * @param maxSize max number of indices in the auxiliary array
 * @param addrSize size of an address
 */
MAGIC MAGICinit(int maxSize, int addrSize)
{
    struct trie *t = (struct trie *)malloc(sizeof *t);
    if (t == NULL)
        goto first_alloc_failed;
    //maxSize * (addrSize + 1) because each entry will have at most addrSize + 1 nodes in the tree.
    //The last + 1 is because of the root node.
    int alloced_memory = maxSize * (addrSize + 1) + 1;
    t->nodes = (struct node *)calloc(alloced_memory, sizeof(struct node));
    if (t->nodes == NULL)
        goto second_alloc_failed;
    //start at 1 to have a root node
    t->nodes_index = 1;
    t->addr_index = 0;
    t->addr_size = addrSize;
    t->max_size = maxSize;
    return (void *)t;

second_alloc_failed:
    free(t);
first_alloc_failed:
    return NULL;
}

/**
 * @brief reset the magic
 * 
 * @param m magic data structure
 */
void MAGICreset(MAGIC m)
{
    struct trie *t = (struct trie *)m;
    t->nodes[0].high = NULL;
    t->nodes[0].eq = NULL;
    t->nodes[0].low = NULL;
    t->nodes_index = 1;
    t->addr_index = 0;
}
/**
 * @brief returns an index 
 * 
 * @param m magic data structure 
 * @param dest destination address
 */
int MAGICindex(MAGIC m, char *dest)
{
    struct trie *t = (struct trie *)m;
    struct node *new_node;
    struct node *current_node = t->nodes;
    int i = 0;
    while (i < t->addr_size) {
        if (current_node->c < dest[i]) {
            if (current_node->low == NULL) {
                new_node = alloc_node(t);
                if (!new_node)
                    return -1;
                new_node->c = dest[i];
                current_node->low = new_node;
            }
            current_node = current_node->low;
        } else if (current_node->c == dest[i]) {
            if (current_node->eq == NULL) {
                new_node = alloc_node(t);
                if (!new_node)
                    return -1;
                if (i + 1 == t->addr_size) {
                    new_node->index = t->addr_index;
                    t->addr_index++;
                } else {
                    new_node->c = dest[i + 1];
                }
                current_node->eq = new_node;
            }
            current_node = current_node->eq;
            i++;
        } else {
            if (current_node->high == NULL) {
                new_node = alloc_node(t);
                if (!new_node)
                    return -1;
                new_node->c = dest[i];
                current_node->high = new_node;
            }
            current_node = current_node->high;
        }
    }
    return current_node->index;
}
/**
 * @brief free the magic data structure
 * 
 * @param m magic data structure to be freed
 */
void MAGICfree(MAGIC m)
{
    struct trie *t = (struct trie *)m;
    free(t->nodes);
    free(t);
}
