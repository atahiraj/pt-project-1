#ifndef _MAGIC_H_
#define _MAGIC_H_

typedef void *MAGIC;

MAGIC MAGICinit(int maxSize, int addrSize);

int MAGICindex(MAGIC m, char *dest);

void MAGICreset(MAGIC m);

void MAGICfree(MAGIC m);
#endif
