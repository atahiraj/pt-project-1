
#ifdef PROGRAM1
#include "MAGIC1/magic.h"
#else
#include "MAGIC2/magic.h"
#endif
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>

#define MAX_SIZE 35
#define ADDR_SIZE 15
#define BATCH_NUMBER 1

/**
 * @brief Running some tests.
 *
 * @return Return value of the tests.
 */
int unit_test()
{
    MAGIC m = MAGICinit(1, 1);
    MAGICindex(m, "a");
    if (MAGICindex(m, "b") != -1) {
        fprintf(stderr, "HTABLE accepted more entries than it should have\n");
        return -1;
    }
    MAGICfree(m);

    m = MAGICinit(MAX_SIZE, ADDR_SIZE);
    char str[ADDR_SIZE + 1];
    str[ADDR_SIZE] = '\0';
    for (unsigned i = 0; i < ADDR_SIZE; ++i)
        str[i] = 1;

    int ret = MAGICindex(m, str);
    int second;
    if (ret != (second = MAGICindex(m, str))) {
        fprintf(stderr, "MAGIC same key, different index first:%d, second:%d\n",
                ret, second);
        return -1;
    }

    for (unsigned i = 1; i < MAX_SIZE; ++i) {
        str[0]++;
        ret = MAGICindex(m, str);
        if ((unsigned)ret != i) {
            printf("%d\n", ret);
            fprintf(
                stderr,
                "MAGIC indexes should be incremented with different keys\n");
            return -1;
        }
    }

    MAGICreset(m);
    char no_null_str[MAX_SIZE];
    for (unsigned i = 0; i < MAX_SIZE; ++i)
        no_null_str[i] = 'x';
    MAGICindex(m, no_null_str);
    MAGICfree(m);

    return 0;
}

int main(int argc, char **argv)
{
    int unit_test_flag = 0;
    int check_flag = 0;
    int addr_size = ADDR_SIZE;
    int max_size = MAX_SIZE;
    int batch_number = BATCH_NUMBER;
    int opt;
    while ((opt = getopt(argc, argv, "ua:m:cb:")) != -1) {
        switch (opt) {
        case 'u':
            unit_test_flag = 1;
            break;
        case 'a':
            addr_size = atoi(optarg);
            break;
        case 'm':
            max_size = atoi(optarg);
            break;
        case 'c':
            check_flag = 1;
            break;
        case 'b':
            batch_number = atoi(optarg);
            break;
        default:
            exit(-1);
        }
    }
    if (unit_test_flag == 1 && unit_test() == -1) {
        fprintf(stderr, "Failed unit tests\n");
        return -1;
    }
    //skip performance test
    if (unit_test_flag == 1)
        exit(0);

    //performance test
    clock_t start, end;
    double cpu_time_used;
    start = clock();

    MAGIC m = MAGICinit(max_size, addr_size);
    char addr[addr_size];
    memset(addr, 0, addr_size);
    FILE *fp = fopen("randomfile", "r");
    for (int i = 0; i < batch_number; i++) {
        for (int i = 0; i < max_size; i++) {
            int red = fread(addr, addr_size, 1, fp);
            if (red < addr_size)
                rewind(fp);
            int addr_idx = MAGICindex(m, addr);
            int second;
            if (check_flag) {
                if ((second = MAGICindex(m, addr)) != addr_idx) {
                    fprintf(
                        stderr,
                        "Error during performance test, could not retrieve right index\n");
                    fprintf(stderr, "Right index: %d, got: %d\n", addr_idx,
                            second);
                    exit(-1);
                }
            }
        }
        MAGICreset(m);
    }
    end = clock();
    cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;

    printf("Time taken: %lf\n", cpu_time_used);
}
