/*!
 * \file magic.c
 * \brief This file implements ad hoc hash tables for the assignement.
 * Collisions are handled with singly linked lists. 
 * \author Tahiraj Arian
 * \author Spits Tanguy
 * \version 0.1
 */

#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "magic.h"

#ifndef HTABLE_BUCKET_RATIO
#define HTABLE_BUCKET_RATIO 3
#endif // HTABLE_BUCKET_RATIO

/*******************************************************************************
 * Useful macros.
 */

/**
 * @brief Expands to offset (in bytes) between a struct and one of its members. 
 * 
 * @param type The struct type.
 * @param member The member name.
 */
#define offset_of(type, member) ((uintptr_t) & (((type *)0)->member))

/**
 * @brief Expands to a pointer to the container of a member.
 *
 * @param ptr Pointer to the member.
 * @param type Type of the struct.
 * @param member Member name.
 */
#define container_of(ptr, type, member)                                        \
    ((type *)((uintptr_t)(ptr)-offset_of(type, member)))

/*******************************************************************************
 * Defining structs.
 */

// Singly linked list element/head.
struct slist_head {
    struct slist_head *next;
};

// Bucket element
struct element {
    int index;
    char *dest;
    struct slist_head list;
    unsigned long batch_nb;
};

// Ad hoc hash table
struct htable {
    // metadata
    int max_size;
    int addr_size;
    int curr_slab_idx;
    unsigned long bucket_count;
    unsigned long curr_batch_nb;

    // data
    char *dest_slab;
    struct element *element_slab;
    struct slist_head *bucket_slab;
};

/*******************************************************************************
 * Singly linked list implementation. We dont need more than these few functions
 * and macros.
 */

/**
 * @brief Adds an element.
 * 
 * @param prev Previous slist head to add a new element after.
 * @param new New slist head to be added.
 */
static inline void slist_add(struct slist_head *prev, struct slist_head *new)
{
    new->next = prev->next;
    prev->next = new;
}

/**
 * @brief Iterate over all element of a list.
 * 
 * @param slh Head of the list to loop over.
 * @param name Name of the (newly created) loop cursor.
 */
#define slist_for_each(slh, name)                                              \
    for (struct slist_head *name = (slh)->next; name; name = name->next)

/*******************************************************************************
 * Hash table implementation
 */

/**
 * @brief Bounded hash function. Source: http://www.cse.yorku.ca/~oz/hash.html
 *
 * @param str The string to be hashed.
 * @param size Upper bound + 1 of the hash.
 * @return The hash of str.
 */
static inline unsigned long hash_function(const unsigned char *str,
                                          const unsigned long size,
                                          const unsigned long bound)
{
    unsigned long hash = 5381;

    for (unsigned long i = 0; i < size; ++i)
        hash = ((hash << 5) + hash) + str[i]; /* hash * 33 + c */

    return hash % bound;
}

/**
 * @brief Returns the element that contains a slist head.
 * 
 * @param sln The slist head to get the container from.
 * @return The element containing sln.
 */
static inline struct element *element_container_of(struct slist_head *sln)
{
    return container_of(sln, struct element, list);
}

/**
 * @brief Returns a new instance of the abstract datatype.
 * 
 * @param maxSize Maximum number of elements.
 * @param addrSize Destination address size.
 * @return A pointer to the newly created MAGIC structure.
 */
MAGIC MAGICinit(const int max_size, const int addr_size)
{
    struct htable *ht = malloc(sizeof(struct htable));

    if (ht == NULL)
        goto magic_out;

    ht->max_size = max_size;
    ht->addr_size = addr_size;
    ht->curr_slab_idx = 0;
    ht->bucket_count = max_size * HTABLE_BUCKET_RATIO;
    ht->curr_batch_nb = 0;

    ht->dest_slab = malloc(max_size * addr_size);
    if (ht->dest_slab == NULL)
        goto magic_dest_slab_fail;

    ht->element_slab = malloc(max_size * sizeof(struct element));
    if (ht->element_slab == NULL)
        goto magic_element_slab_fail;

    ht->bucket_slab = calloc(ht->bucket_count, sizeof(struct slist_head));
    if (ht->bucket_slab == NULL)
        goto magic_bucket_slab_fail;

    return ht;

magic_bucket_slab_fail:
    free(ht->element_slab);

magic_element_slab_fail:
    free(ht->dest_slab);

magic_dest_slab_fail:
    free(ht);

magic_out:
    return NULL;
}

/**
 * @brief Returns the index in the auxiliary array of a given address.
 * 
 * @param m Pointer to a MAGIC structure.
 * @param dest Destination address.
 * @return Corresponding index.
 */
int MAGICindex(MAGIC m, const char *const dest)
{
    struct htable *ht = (struct htable *)m;

    // Searching for the bucket and any search hit.
    unsigned long hash = hash_function((const unsigned char *)dest,
                                       ht->addr_size, ht->bucket_count);
    struct slist_head *bucket_head = &ht->bucket_slab[hash];
    slist_for_each(bucket_head, sln)
    {
        struct element *elm = element_container_of(sln);
        if (elm->batch_nb != ht->curr_batch_nb) {
            bucket_head->next = NULL;
            break;
        }

        if (!strncmp(dest, elm->dest, ht->addr_size))
            return elm->index;
    }

    if (ht->curr_slab_idx == ht->max_size)
        return -1;

    // At this point, there was a search miss. Next element in the slab
    // is allocated and added at the head of the bucket.
    struct element *new_elm = &ht->element_slab[ht->curr_slab_idx];
    new_elm->dest = &ht->dest_slab[ht->curr_slab_idx * ht->addr_size];
    new_elm->index = ht->curr_slab_idx;
    new_elm->batch_nb = ht->curr_batch_nb;
    strncpy(new_elm->dest, dest, ht->addr_size);

    slist_add(bucket_head, &new_elm->list);

    return ht->curr_slab_idx++;
}

/**
 * @brief Resets a MAGIC structure.
 * 
 * @param m Pointer to a MAGIC structure to be reset.
 */
void MAGICreset(MAGIC m)
{
    struct htable *ht = (struct htable *)m;
    ht->curr_slab_idx = 0;
    ht->curr_batch_nb++;
}

/**
 * @brief Frees a hash table.
 *
 * @param m The htable to be freed.
 */
void MAGICfree(MAGIC m)
{
    struct htable *ht = (struct htable *)m;
    free(ht->bucket_slab);
    free(ht->element_slab);
    free(ht->dest_slab);
    free(ht);
}
