/*!
 * \file magic.h
 * \brief MAGIC ADT interface.
 * \author Tahiraj Arian
 * \author Spits Tanguy
 * \version 0.1
 */

#ifndef MAGIC_H
#define MAGIC_H

// Opaque pointer to an opaque structure.
typedef void *MAGIC;

/**
 * @brief Returns a new instance of the abstract datatype.
 * 
 * @param maxSize Maximum number of elements.
 * @param addrSize Destination address size.
 * @return A pointer to the newly created MAGIC structure.
 */
MAGIC MAGICinit(const int maxSize, const int addrSize);

/**
 * @brief Returns the index in the auxiliary array of a given address.
 * 
 * @param m Pointer to a MAGIC structure.
 * @param dest Destination address.
 * @return Corresponding index.
 */
int MAGICindex(MAGIC m, const char *const dest);

/**
 * @brief Resets a MAGIC structure.
 * 
 * @param m Pointer to a MAGIC structure to be reset.
 */
void MAGICreset(MAGIC m);

/**
 * @brief Frees a MAGIC structure.
 * 
 * @param m Pointer to a MAGIC structure to be freed.
 */
void MAGICfree(MAGIC m);

#endif
