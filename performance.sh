#!/bin/bash

for b in 1 10 100 1000; do
    for a in 1 2 4 8 16 32; do
        for m in 100 1000 10000 100000 1000000; do
            echo -n "1: $b $a $m :";
            ./program1 -b$b -a$a -m$m
            echo -n "2: $b $a $m :";
            ./program2 -b$b -a$a -m$m
        done
    done
done
