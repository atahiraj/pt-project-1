#!/usr/bin/gnuplot

set term png
set output "plot.png"

set ylabel "time(seconds)"
set xlabel "maximum size"

set title "Comparison of the two implementations\n with a variable max size, key length=16 bytes and 1000 resets"
set xtics rotate by -45
set grid
plot "./perf.dat" using 1:2 with linespoints title "MAGIC1","./perf.dat" using 1:3 with linespoints title "MAGIC2"