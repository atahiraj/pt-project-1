#!/usr/bin/bash

for m in 1000 10000 100000 250000 350000 500000 650000 800000 900000 ; do
    time1=$(./program1 -a16 -b1000 -m$m | egrep -o "[[:digit:]]+\.[[:digit:]]*")
    time2=$(./program2 -a16 -b1000 -m$m | egrep -o "[[:digit:]]+\.[[:digit:]]*")
    echo $m, $time1, $time2 >> perf.dat
done
for a in 1 2 4 8 16 32 64 128; do
    time1=$(./program1 -a$a -b1000 -m100000 | egrep -o "[[:digit:]]+\.[[:digit:]]*")
    time2=$(./program2 -a$a -b1000 -m100000 | egrep -o "[[:digit:]]+\.[[:digit:]]*")
    echo $a, $time1, $time2 >> perf2.dat
done