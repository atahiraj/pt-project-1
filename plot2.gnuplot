#!/usr/bin/gnuplot

set term png
set output "plot2.png"

set ylabel "time(seconds)"
set xlabel "address size(bytes)"

set title "Comparison of the two implementations\n with a variable address size, max size of 100000 and 1000 resets"

plot "./perf2.dat" using 1:2 with linespoints title "MAGIC1","./perf2.dat" using 1:3 with linespoints title "MAGIC2"
